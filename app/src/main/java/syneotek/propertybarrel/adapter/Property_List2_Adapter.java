package syneotek.propertybarrel.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.chauthai.swipereveallayout.ViewBinderHelper;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.daimajia.swipe.implments.SwipeItemRecyclerMangerImpl;

import java.util.ArrayList;

import syneotek.propertybarrel.R;
import syneotek.propertybarrel.module.PropertyList_Items;

/**
 * Created by Syneotek on 02/05/2017.
 */

public class Property_List2_Adapter extends RecyclerSwipeAdapter<Property_List2_Adapter.MyViewHolder> {

    Activity activity;
    ArrayList<PropertyList_Items> arr_property_list = new ArrayList<PropertyList_Items>();

    public SwipeItemRecyclerMangerImpl mItemManger;

    public Property_List2_Adapter(Activity activity, ArrayList<PropertyList_Items> arr_property_list) {
        this.activity = activity;
        this.arr_property_list = arr_property_list;

        mItemManger = new SwipeItemRecyclerMangerImpl(this);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adpt_property_list2_items, parent, false);

        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout rele_discard,rele_favorite;
        RelativeLayout rele_main_layout;
        SwipeLayout swipeLayout;

        public MyViewHolder(View view) {
            super(view);

            swipeLayout = (SwipeLayout) view.findViewById(R.id.swipe);
            rele_discard = (RelativeLayout) view.findViewById(R.id.rele_discard);
            rele_favorite = (RelativeLayout) view.findViewById(R.id.rele_favorite);
            rele_main_layout = (RelativeLayout) view.findViewById(R.id.rele_main_layout);
        }
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        PropertyList_Items propertyList_items = arr_property_list.get(position);


        holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);

        // Drag From Left
        holder.swipeLayout.addDrag(SwipeLayout.DragEdge.Left, holder.swipeLayout.findViewById(R.id.bottom_wrapper1));

        // Drag From Right
        holder.swipeLayout.addDrag(SwipeLayout.DragEdge.Right, holder.swipeLayout.findViewById(R.id.bottom_wrapper));


        // Handling different events when swiping
        holder.swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override
            public void onClose(SwipeLayout layout) {
                //when the SurfaceView totally cover the BottomView.
            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {
                //you are swiping.
            }

            @Override
            public void onStartOpen(SwipeLayout layout) {

            }

            @Override
            public void onOpen(SwipeLayout layout) {
                //when the BottomView totally show.
            }

            @Override
            public void onStartClose(SwipeLayout layout) {

            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {
                //when user's hand released.
            }
        });


        holder.rele_discard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(activity, position + " Discard", Toast.LENGTH_SHORT).show();
            }
        });
       holder.rele_favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(activity, position + " Favorite", Toast.LENGTH_SHORT).show();
            }
        });


        // mItemManger is member in RecyclerSwipeAdapter Class
        mItemManger.bindView(holder.itemView, position);

    }

    @Override
    public int getItemCount() {
        return arr_property_list.size();
    }


    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }
}



//package syneotek.propertybarrel.adapter;
//
//        import android.app.Activity;
//        import android.support.v7.widget.RecyclerView;
//        import android.view.LayoutInflater;
//        import android.view.View;
//        import android.view.ViewGroup;
//        import android.widget.RelativeLayout;
//        import android.widget.Toast;
//
//        import com.chauthai.swipereveallayout.SwipeRevealLayout;
//        import com.chauthai.swipereveallayout.ViewBinderHelper;
//
//        import java.util.ArrayList;
//
//        import syneotek.propertybarrel.R;
//        import syneotek.propertybarrel.module.PropertyList_Items;
//
///**
// * Created by Syneotek on 02/05/2017.
// */
//
//public class Property_List2_Adapter extends RecyclerView.Adapter<Property_List2_Adapter.MyViewHolder> {
//
//    Activity activity;
//    ArrayList<PropertyList_Items> arr_property_list = new ArrayList<PropertyList_Items>();
//    private final ViewBinderHelper viewBinderHelper = new ViewBinderHelper();
//
//    public Property_List2_Adapter(Activity activity, ArrayList<PropertyList_Items> arr_property_list) {
//        this.activity = activity;
//        this.arr_property_list = arr_property_list;
//    }
//
//    @Override
//    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adpt_property_list2_items, parent, false);
//
//        MyViewHolder holder = new MyViewHolder(view);
//
//        return holder;
//    }
//
//    public class MyViewHolder extends RecyclerView.ViewHolder {
//
//        RelativeLayout rele_discard;
//        SwipeRevealLayout swipeRevealLayout;
//        RelativeLayout rele_main_layout;
//
//        public MyViewHolder(View view) {
//            super(view);
//
//            rele_discard = (RelativeLayout) view.findViewById(R.id.rele_discard);
//            swipeRevealLayout = (SwipeRevealLayout) view.findViewById(R.id.swipeRevealLayout);
//            rele_main_layout = (RelativeLayout) view.findViewById(R.id.rele_main_layout);
//        }
//    }
//
//    @Override
//    public void onBindViewHolder(MyViewHolder holder, final int position) {
//
//        PropertyList_Items propertyList_items = arr_property_list.get(position);
//
//        holder.rele_discard.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Toast.makeText(activity, position + " Discard", Toast.LENGTH_SHORT).show();
//            }
//        });
//
//
//        viewBinderHelper.bind(holder.swipeRevealLayout, propertyList_items.id);
//    }
//
//    @Override
//    public int getItemCount() {
//        return arr_property_list.size();
//    }
//}
