package syneotek.propertybarrel.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import syneotek.propertybarrel.MainActivity;
import syneotek.propertybarrel.R;
import syneotek.propertybarrel.fragment.Frag_Map_view;
import syneotek.propertybarrel.fragment.Frag_Specific_Listing;

/**
 * Created by Syneotek on 02/05/2017.
 */

public class Property_List_Adapter extends RecyclerView.Adapter<Property_List_Adapter.MyViewHolder> {

    Activity activity;

    public Property_List_Adapter(Activity activity) {
        this.activity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adpt_property_list_items, parent, false);

        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        LinearLayout line_bottom;
        RelativeLayout rele_detail;
        ImageView img_home;

        public MyViewHolder(View view) {
            super(view);

            line_bottom = (LinearLayout) view.findViewById(R.id.line_bottom);
            rele_detail = (RelativeLayout) view.findViewById(R.id.rele_detail);
            img_home = (ImageView) view.findViewById(R.id.img_home);

        }
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.line_bottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((MainActivity) activity).replaceFragment(new Frag_Map_view());
            }
        });

        holder.rele_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((MainActivity) activity).replaceFragment(new Frag_Specific_Listing());

            }
        });

        holder.img_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((MainActivity) activity).replaceFragment(new Frag_Specific_Listing());

            }
        });

    }

    @Override
    public int getItemCount() {
        return 15;
    }
}
