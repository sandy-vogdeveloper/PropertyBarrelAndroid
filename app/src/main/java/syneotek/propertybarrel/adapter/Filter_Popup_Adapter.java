package syneotek.propertybarrel.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;

import syneotek.propertybarrel.R;
import syneotek.propertybarrel.module.Module_Popup_Filter;

/**
 * Created by Syneotek on 02/05/2017.
 */

public class Filter_Popup_Adapter extends RecyclerView.Adapter<Filter_Popup_Adapter.MyViewHolder> {

    Activity activity;
    private ArrayList<Module_Popup_Filter> arr_prprty_type = new ArrayList<>();

    public Filter_Popup_Adapter(Activity activity, ArrayList<Module_Popup_Filter> arr_prprty_type ) {
        this.activity = activity;
        this.arr_prprty_type = arr_prprty_type;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adpt_popup_filter, parent, false);

        MyViewHolder holder = new MyViewHolder(view);
        holder.setIsRecyclable(false);

        return holder;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txt_name;
        ImageButton imgbtn_circle;

        public MyViewHolder(View view) {
            super(view);

            txt_name=(TextView)view.findViewById(R.id.txt_name);
            imgbtn_circle=(ImageButton) view.findViewById(R.id.imgbtn_circle);

        }
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final Module_Popup_Filter filter=arr_prprty_type.get(position);
        holder.txt_name.setText(filter.name);

        if (filter.circl_status==1)
            holder.imgbtn_circle.setBackgroundResource(R.drawable.ic_dot_fill);


        holder.imgbtn_circle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int s=0;s<arr_prprty_type.size();s++)
                {
                        arr_prprty_type.get(s).circl_status=0;
                }

                arr_prprty_type.get(position).circl_status=1;
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return arr_prprty_type.size();
//        return arr_menu_name.length;
    }
}
