package syneotek.propertybarrel.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;

import java.util.ArrayList;

import syneotek.propertybarrel.MainActivity;
import syneotek.propertybarrel.R;
import syneotek.propertybarrel.fragment.Frag_Agent_Profile;
import syneotek.propertybarrel.module.PropertyList_Items;

public class Agent_List_Adapter extends RecyclerSwipeAdapter<Agent_List_Adapter.SimpleViewHolder> {

//    private Context mContext;
//    private ArrayList<Student> studentList;
    Activity activity;
    ArrayList<PropertyList_Items> arr_property_list = new ArrayList<PropertyList_Items>();


    public Agent_List_Adapter(Activity context, ArrayList<PropertyList_Items> arr_property_list) {
        this.activity = context;
        this.arr_property_list = arr_property_list;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adpt_agent_list_items, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder viewHolder, final int position) {
        final PropertyList_Items propertyList_items = arr_property_list.get(position);

//        viewHolder.tvName.setText((item.getName()) + "  -  Row Position " + position);
//        viewHolder.tvEmailId.setText(item.getEmailId());


        viewHolder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);

        // Drag From Left
        viewHolder.swipeLayout.addDrag(SwipeLayout.DragEdge.Left, viewHolder.swipeLayout.findViewById(R.id.bottom_wrapper1));

        // Drag From Right
//        viewHolder.swipeLayout.addDrag(SwipeLayout.DragEdge.Right, viewHolder.swipeLayout.findViewById(R.id.bottom_wrapper));


        // Handling different events when swiping
        viewHolder.swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override
            public void onClose(SwipeLayout layout) {
                //when the SurfaceView totally cover the BottomView.
            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {
                //you are swiping.
            }

            @Override
            public void onStartOpen(SwipeLayout layout) {

            }

            @Override
            public void onOpen(SwipeLayout layout) {
                //when the BottomView totally show.
            }

            @Override
            public void onStartClose(SwipeLayout layout) {

            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {
                //when user's hand released.
            }
        });

        /*viewHolder.swipeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if ((((SwipeLayout) v).getOpenStatus() == SwipeLayout.Status.Close)) {
                    //Start your activity

                    Toast.makeText(mContext, " onClick : " + item.getName() + " \n" + item.getEmailId(), Toast.LENGTH_SHORT).show();
                }

            }
        });*/

        viewHolder.rele_main_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)activity).replaceFragment(new Frag_Agent_Profile());
            }
        });

        viewHolder.swipeLayout.getSurfaceView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(activity, " onClick : " + position, Toast.LENGTH_SHORT).show();
            }
        });


        viewHolder.rele_discard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(v.getContext(), "Discard", Toast.LENGTH_SHORT).show();
            }
        });


        viewHolder.rele_favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(view.getContext(), "Favorite", Toast.LENGTH_SHORT).show();
            }
        });

//        viewHolder.tvEdit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Toast.makeText(view.getContext(), "Clicked on Edit  " + viewHolder.tvName.getText().toString(), Toast.LENGTH_SHORT).show();
//            }
//        });
//
//
//        viewHolder.tvDelete.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mItemManger.removeShownLayouts(viewHolder.swipeLayout);
//                studentList.remove(position);
//                notifyItemRemoved(position);
//                notifyItemRangeChanged(position, studentList.size());
//                mItemManger.closeAllItems();
//                Toast.makeText(view.getContext(), "Deleted " + viewHolder.tvName.getText().toString(), Toast.LENGTH_SHORT).show();
//            }
//        });


        // mItemManger is member in RecyclerSwipeAdapter Class
        mItemManger.bindView(viewHolder.itemView, position);

    }

    @Override
    public int getItemCount() {
        return arr_property_list.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }


    //  ViewHolder Class

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {
        SwipeLayout swipeLayout;
        //        TextView tvName;
//        TextView tvEmailId;
//        TextView tvDelete;
//        TextView tvEdit;
//        TextView tvShare;
//        ImageButton btnLocation;
        RelativeLayout rele_discard, rele_favorite;
        RelativeLayout rele_main_layout;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            rele_discard = (RelativeLayout) itemView.findViewById(R.id.rele_discard);
            rele_favorite = (RelativeLayout) itemView.findViewById(R.id.rele_favorite);
            rele_main_layout = (RelativeLayout) itemView.findViewById(R.id.rele_main_layout);

//            tvName = (TextView) itemView.findViewById(R.id.tvName);
//            tvEmailId = (TextView) itemView.findViewById(R.id.tvEmailId);
//            tvDelete = (TextView) itemView.findViewById(R.id.tvDelete);
//            tvEdit = (TextView) itemView.findViewById(R.id.tvEdit);
//            tvShare = (TextView) itemView.findViewById(R.id.tvShare);
//            btnLocation = (ImageButton) itemView.findViewById(R.id.btnLocation);


        }
    }
}
