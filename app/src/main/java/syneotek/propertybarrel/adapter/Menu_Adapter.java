package syneotek.propertybarrel.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import syneotek.propertybarrel.R;

/**
 * Created by Syneotek on 02/05/2017.
 */

public class Menu_Adapter extends RecyclerView.Adapter<Menu_Adapter.MyViewHolder> {

    Activity activity;
    String [] arr_menu_name={};
    int [] arr_menu_image={};
    int menu_count;

    public Menu_Adapter(Activity activity,String [] arr_menu_name,int [] arr_menu_image,int menu_count) {
        this.activity = activity;
        this.arr_menu_name = arr_menu_name;
        this.arr_menu_image = arr_menu_image;
        this.menu_count = menu_count;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adpt_menu_list_items, parent, false);

        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txt_menu_name;
        ImageView img_menu;

        public MyViewHolder(View view) {
            super(view);

            txt_menu_name=(TextView)view.findViewById(R.id.txt_menu_name);
            img_menu=(ImageView) view.findViewById(R.id.img_menu);

        }
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.img_menu.setImageResource(arr_menu_image[position]);
        holder.txt_menu_name.setText(arr_menu_name[position]);
    }

    @Override
    public int getItemCount() {
        return menu_count;
//        return arr_menu_name.length;
    }
}
