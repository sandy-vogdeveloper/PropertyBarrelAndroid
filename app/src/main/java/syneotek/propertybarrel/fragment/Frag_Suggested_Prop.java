package syneotek.propertybarrel.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.daimajia.swipe.util.Attributes;

import java.util.ArrayList;

import syneotek.propertybarrel.R;
import syneotek.propertybarrel.adapter.Favorite_List_Adapter;
import syneotek.propertybarrel.adapter.Suggested_List_Adapter;
import syneotek.propertybarrel.module.PropertyList_Items;


/**
 * Created by Syneotek1 on 9/12/2016.
 */
public class Frag_Suggested_Prop extends Fragment {

    RecyclerView recycler_property_list;
    RecyclerView.LayoutManager layoutManager;

    View view;
    ArrayList<PropertyList_Items> arr_property_list = new ArrayList<PropertyList_Items>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.frag_suggested_list, container, false);

        recycler_property_list = (RecyclerView) view.findViewById(R.id.recycler_property_list);
        layoutManager = new LinearLayoutManager(getActivity());

        for (int i = 0; i < 10; i++) {

            PropertyList_Items propertyList_items = new PropertyList_Items();
            propertyList_items.id = "" + i;
            propertyList_items.distance = i + " km";

            arr_property_list.add(propertyList_items);
        }

        // Creating Adapter object
        Suggested_List_Adapter mAdapter = new Suggested_List_Adapter(getActivity(), arr_property_list);

        // Setting Mode to Single to reveal bottom View for one item in List
        // Setting Mode to Mutliple to reveal bottom Views for multile items in List
        ((Suggested_List_Adapter) mAdapter).setMode(Attributes.Mode.Single);

        recycler_property_list.setHasFixedSize(true);
        recycler_property_list.setLayoutManager(layoutManager);
        recycler_property_list.setAdapter(mAdapter);

//        recycler_property_list.setAdapter(new Property_List2_Adapter(getActivity(),arr_property_list));

        manageView();
        return view;
    }


    public void manageView() {


    }
}