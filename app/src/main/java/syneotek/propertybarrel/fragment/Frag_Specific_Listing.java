package syneotek.propertybarrel.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import java.util.Calendar;

import syneotek.propertybarrel.MainActivity;
import syneotek.propertybarrel.R;


/**
 * Created by Syneotek1 on 9/12/2016.
 */
public class Frag_Specific_Listing extends Fragment {

    View view;
    TextView txt_back;

    CarouselView carouselView;
    int[] sampleImages = {R.drawable.index1, R.drawable.index2, R.drawable.index3, R.drawable.images4, R.drawable.images5, R.drawable.images7, R.drawable.images8};
    TextView txt_count;

    ImageButton next, prev,imgbtn_show;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.frag_specific_listin, container, false);

        txt_back = (TextView) view.findViewById(R.id.txt_back);

        txt_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).replaceFragment(new Frag_Property_List());
            }
        });


        carouselView = (CarouselView) view.findViewById(R.id.carouselView);
        carouselView.setPageCount(sampleImages.length);

        txt_count = (TextView) view.findViewById(R.id.txt_count);
        carouselView.setImageListener(imageListener);

        carouselView.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                int s = position + 1;
                txt_count.setText(" " + s + "/" + sampleImages.length + " ");
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        next = (ImageButton) view.findViewById(R.id.next);
        prev = (ImageButton) view.findViewById(R.id.prev);
        imgbtn_show = (ImageButton) view.findViewById(R.id.imgbtn_show);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (carouselView.getCurrentItem() < sampleImages.length)
                carouselView.setCurrentItem(carouselView.getCurrentItem() + 1);

            }
        });

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (carouselView.getCurrentItem() > 0)
                    carouselView.setCurrentItem(carouselView.getCurrentItem() - 1);
            }
        });

        imgbtn_show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bookviewDialog();
            }
        });

        return view;
    }

    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            imageView.setImageResource(sampleImages[position]);
        }
    };

    public void bookviewDialog()
    {

        Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_book_viewing);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        final String[] values = {"1.00 pm", "2.00 pm", "3.00 pm", "4.00 pm", "5.00 pm", "6.00 pm", "7.00 pm", "8.00 pm", "9.00 pm", "10.00 pm", "11.00 pm", "12.00 pm", "1.00 am", "2.00 am", "3.00 am", "4.00 am", "5.00 am", "6.00 am", "7.00 am", "8.00 am", "9.00 am", "10.00 am", "11.00 am", "12.00 am"};
        NumberPicker np_condo = (NumberPicker) dialog.findViewById(R.id.np_bookview);
        np_condo.setMinValue(0);
        np_condo.setMaxValue(values.length - 1);
        np_condo.setDisplayedValues(values);
        np_condo.setWrapSelectorWheel(true);


        NumberPicker np_condo2 = (NumberPicker) dialog.findViewById(R.id.np_bookview2);
        np_condo2.setMinValue(0);
        np_condo2.setMaxValue(values.length - 1);
        np_condo2.setDisplayedValues(values);
        np_condo2.setWrapSelectorWheel(true);

        final TextView txt_day = (TextView) dialog.findViewById(R.id.txt_day);

        txt_day.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();

                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                        txt_day.setText(dayOfMonth + "-" + (month + 1) + "-" + year);
                    }
                }, mYear, mMonth, mDay);
                datePickerDialog.show();

            }
        });

        dialog.show();
    }

}