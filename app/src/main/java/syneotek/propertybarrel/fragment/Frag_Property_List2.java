package syneotek.propertybarrel.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.swipe.util.Attributes;

import java.util.ArrayList;

import syneotek.propertybarrel.MainActivity;
import syneotek.propertybarrel.R;
import syneotek.propertybarrel.adapter.Property_List2_Adapter;
import syneotek.propertybarrel.module.PropertyList_Items;
import syneotek.propertybarrel.testing.SwipeRecyclerViewAdapter;
import syneotek.propertybarrel.testing.SwipeRecyclerViewAdapter2;


/**
 * Created by Syneotek1 on 9/12/2016.
 */
public class Frag_Property_List2 extends Fragment {

    RecyclerView recycler_property_list;
    RecyclerView.LayoutManager layoutManager;

    ImageButton imgbtn_refresh, imgbtn_fav;
    View view;

    ArrayList<PropertyList_Items> arr_property_list = new ArrayList<PropertyList_Items>();

    ImageButton filter_on_filter, filter_list2;
    RelativeLayout filter_view;
    RelativeLayout pro_type, price, beds, bath, open_hs, condo_fee, status, area;
    TextView txt_filt_site;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.frag_property_list2, container, false);

        recycler_property_list = (RecyclerView) view.findViewById(R.id.recycler_property_list);
        layoutManager = new LinearLayoutManager(getActivity());

        for (int i = 0; i < 10; i++) {

            PropertyList_Items propertyList_items = new PropertyList_Items();
            propertyList_items.id = "" + i;
            propertyList_items.distance = i + " km";

            arr_property_list.add(propertyList_items);
        }

        // Creating Adapter object
        SwipeRecyclerViewAdapter mAdapter = new SwipeRecyclerViewAdapter(getActivity(), arr_property_list);


        // Setting Mode to Single to reveal bottom View for one item in List
        // Setting Mode to Mutliple to reveal bottom Views for multile items in List
        ((SwipeRecyclerViewAdapter) mAdapter).setMode(Attributes.Mode.Single);

        recycler_property_list.setHasFixedSize(true);
        recycler_property_list.setLayoutManager(layoutManager);
        recycler_property_list.setAdapter(mAdapter);

//        recycler_property_list.setAdapter(new Property_List2_Adapter(getActivity(),arr_property_list));

        manageView();
        return view;
    }


    public void manageView() {

        //--------------------------------filter view -------------------------

        txt_filt_site=(TextView)view.findViewById(R.id.txt_filt_site);

        filter_on_filter = (ImageButton) view.findViewById(R.id.filter_on_filter);
        filter_list2 = (ImageButton) view.findViewById(R.id.filter_list2);
        filter_view = (RelativeLayout) view.findViewById(R.id.filter_view);
        filter_view.setVisibility(View.GONE);

        txt_filt_site.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                filter_view.setVisibility(View.GONE);
            }
        });

        filter_on_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Animation fadeInAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.fadeout);
//                // Now Set your animation
//                filter_view.startAnimation(fadeInAnimation);
                filter_view.setVisibility(View.GONE);

            }
        });

        filter_list2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                filter_view.setVisibility(View.VISIBLE);
                Animation fadeInAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.fadein);
               // Now Set your animation
                filter_view.startAnimation(fadeInAnimation);

            }
        });

        pro_type = (RelativeLayout) view.findViewById(R.id.rele1);
        price = (RelativeLayout) view.findViewById(R.id.rele2);
        beds = (RelativeLayout) view.findViewById(R.id.rele3);
        bath = (RelativeLayout) view.findViewById(R.id.rele4);
        open_hs = (RelativeLayout) view.findViewById(R.id.rele5);
        condo_fee = (RelativeLayout) view.findViewById(R.id.rele6);
        status = (RelativeLayout) view.findViewById(R.id.rele7);
        area = (RelativeLayout) view.findViewById(R.id.rele8);

        pro_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        price.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        beds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        bath.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        open_hs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        condo_fee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        area.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        //-----------------------------------------------------------------

        imgbtn_refresh = (ImageButton) view.findViewById(R.id.imgbtn_refresh);
        imgbtn_fav = (ImageButton) view.findViewById(R.id.imgbtn_fav);

        imgbtn_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ((MainActivity) getActivity()).replaceFragment(new Frag_Property_List());
            }
        });

        imgbtn_fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ((MainActivity) getActivity()).replaceFragment(new Frag_Favorites_List());
            }
        });
    }
}