package syneotek.propertybarrel.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import syneotek.propertybarrel.MainActivity;
import syneotek.propertybarrel.R;


/**
 * Created by Syneotek1 on 9/12/2016.
 */
public class Frag_Agent_Profile extends Fragment {

    View view;
    TextView txt_back;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.agent_profile, container, false);

        txt_back = (TextView) view.findViewById(R.id.txt_back);

        txt_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).replaceFragment(new Frag_Select_Agent());
            }
        });

        return view;
    }


}