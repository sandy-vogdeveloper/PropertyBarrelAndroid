package syneotek.propertybarrel.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.swipe.util.Attributes;

import java.util.ArrayList;

import syneotek.propertybarrel.MainActivity;
import syneotek.propertybarrel.R;
import syneotek.propertybarrel.adapter.Favorite_List_Adapter;
import syneotek.propertybarrel.module.PropertyList_Items;
import syneotek.propertybarrel.testing.SwipeRecyclerViewAdapter;


/**
 * Created by Syneotek1 on 9/12/2016.
 */
public class Frag_Favorites_List extends Fragment {

    RecyclerView recycler_property_list;
    RecyclerView.LayoutManager layoutManager;

    View view;

    ArrayList<PropertyList_Items> arr_property_list = new ArrayList<PropertyList_Items>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.frag_favorites_list, container, false);

        recycler_property_list = (RecyclerView) view.findViewById(R.id.recycler_property_list);
        layoutManager = new LinearLayoutManager(getActivity());

        for (int i = 0; i < 10; i++) {

            PropertyList_Items propertyList_items = new PropertyList_Items();
            propertyList_items.id = "" + i;
            propertyList_items.distance = i + " km";

            arr_property_list.add(propertyList_items);
        }

        // Creating Adapter object
        Favorite_List_Adapter mAdapter = new Favorite_List_Adapter(getActivity(), arr_property_list);

        // Setting Mode to Single to reveal bottom View for one item in List
        // Setting Mode to Mutliple to reveal bottom Views for multile items in List
        ((Favorite_List_Adapter) mAdapter).setMode(Attributes.Mode.Single);

        recycler_property_list.setHasFixedSize(true);
        recycler_property_list.setLayoutManager(layoutManager);
        recycler_property_list.setAdapter(mAdapter);

//        recycler_property_list.setAdapter(new Property_List2_Adapter(getActivity(),arr_property_list));

        manageView();
        return view;
    }


    public void manageView() {


    }
}