package syneotek.propertybarrel;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import syneotek.propertybarrel.adapter.Menu_Adapter;
import syneotek.propertybarrel.adapter.Property_List_Adapter;
import syneotek.propertybarrel.fragment.Frag_Favorites_List;
import syneotek.propertybarrel.fragment.Frag_Messages;
import syneotek.propertybarrel.fragment.Frag_Mrtg_Calculator;
import syneotek.propertybarrel.fragment.Frag_Property_List;
import syneotek.propertybarrel.fragment.Frag_Select_Agent;
import syneotek.propertybarrel.fragment.Frag_Settings;
import syneotek.propertybarrel.fragment.Frag_Share;
import syneotek.propertybarrel.fragment.Frag_Suggested_Prop;
import syneotek.propertybarrel.fragment_agent.Frag_Property_List_Agent;
import syneotek.propertybarrel.util.RecyclerItemClickListener;

public class MainActivity extends AppCompatActivity {

    RecyclerView recycler_menu;
    RecyclerView.LayoutManager layoutManager;

    String[] arr_menu_name = {"Property Search", "Favorites", "Messaging", "Menu", "Mortgage Calculator", "Find a Agent", "Map", "Suggested Properties", "Settings", "Share App"};
    int[] arr_menu_image = {R.drawable.men_search, R.drawable.men_fev, R.drawable.men_msg, R.drawable.men_menu,
            R.drawable.men_calc, R.drawable.men_agent, R.drawable.men_mapicon, R.drawable.men_home, R.drawable.men_setting, R.drawable.men_shearapp};

    int menu_count = 4;

    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recycler_menu = (RecyclerView) findViewById(R.id.recycler_menu);
        layoutManager = new LinearLayoutManager(this);

        recycler_menu.setHasFixedSize(true);
        recycler_menu.setLayoutManager(layoutManager);
        recycler_menu.setAdapter(new Property_List_Adapter(MainActivity.this));

        recycler_menu = (RecyclerView) findViewById(R.id.recycler_menu);
        layoutManager = new GridLayoutManager(this, 4);

        recycler_menu.setHasFixedSize(true);
        recycler_menu.setLayoutManager(layoutManager);
        recycler_menu.setAdapter(new Menu_Adapter(MainActivity.this, arr_menu_name, arr_menu_image, menu_count));

        recycler_menu.addOnItemTouchListener(new RecyclerItemClickListener(MainActivity.this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, final int position) {

                switch (position) {

                    case 0:
                        replaceFragment(new Frag_Property_List());
                        break;

                    case 1:
                        replaceFragment(new Frag_Favorites_List());
                        break;

                    case 2:
                        replaceFragment(new Frag_Messages());
                        break;

                    case 3:
                        if (menu_count == 4)
                            menu_count = 10;
                        else
                            menu_count = 4;

                        recycler_menu.setAdapter(new Menu_Adapter(MainActivity.this, arr_menu_name, arr_menu_image, menu_count));
                        break;

                    case 4:
                        replaceFragment(new Frag_Mrtg_Calculator());
                        menu_count = 4;
                        recycler_menu.setAdapter(new Menu_Adapter(MainActivity.this, arr_menu_name, arr_menu_image, menu_count));
                        break;
                    case 5:
                        replaceFragment(new Frag_Select_Agent());
                        menu_count = 4;
                        recycler_menu.setAdapter(new Menu_Adapter(MainActivity.this, arr_menu_name, arr_menu_image, menu_count));
                        break;

                    case 6:
                        intent = new Intent(MainActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();
                        break;

                    case 7:
                        replaceFragment(new Frag_Suggested_Prop());
                        menu_count = 4;
                        recycler_menu.setAdapter(new Menu_Adapter(MainActivity.this, arr_menu_name, arr_menu_image, menu_count));
                        break;

                    case 8:
                        replaceFragment(new Frag_Settings());
                        menu_count = 4;
                        recycler_menu.setAdapter(new Menu_Adapter(MainActivity.this, arr_menu_name, arr_menu_image, menu_count));
                        break;

                    case 9:
                        replaceFragment(new Frag_Share());
                        menu_count = 4;
                        recycler_menu.setAdapter(new Menu_Adapter(MainActivity.this, arr_menu_name, arr_menu_image, menu_count));
                        break;
                }

            }
        }));

        replaceFragment(new Frag_Property_List());
    }

    public void replaceFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_main, fragment);
        fragmentManager.popBackStackImmediate();
        fragmentTransaction.commit();
    }

}


//    RecyclerView recycler_menu;
//    RecyclerView.LayoutManager layoutManager;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//
//        recycler_menu = (RecyclerView) findViewById(R.id.recycler_menu);
//        layoutManager = new LinearLayoutManager(this);
//
//        recycler_menu.setHasFixedSize(true);
//        recycler_menu.setLayoutManager(layoutManager);
//        recycler_menu.setAdapter(new Property_List_Adapter(MainActivity.this));
//    }