package syneotek.propertybarrel.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import syneotek.propertybarrel.HomeActivity;
import syneotek.propertybarrel.R;
import syneotek.propertybarrel.activity_agent.Agent_HomeActivity;

public class LoginActivity extends AppCompatActivity {

    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    public void goLogin(View view)
    {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_choose);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView txt_user=(TextView)dialog.findViewById(R.id.txt_user);
        TextView txt_agent=(TextView)dialog.findViewById(R.id.txt_agent);

        txt_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                intent = new Intent(LoginActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        });

        txt_agent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                intent = new Intent(LoginActivity.this, Agent_HomeActivity.class);
                startActivity(intent);
            }
        });

        dialog.show();
    }

    public void goSignup(View view)
    {
        intent = new Intent(LoginActivity.this, RegisterActivity.class);
        startActivity(intent);
    }
}
