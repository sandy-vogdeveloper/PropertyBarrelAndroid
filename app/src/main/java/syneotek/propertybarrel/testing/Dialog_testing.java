package syneotek.propertybarrel.testing;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.NumberPicker;
import android.widget.TextView;

import java.util.Calendar;

import syneotek.propertybarrel.R;
import syneotek.propertybarrel.adapter_agent.Suggest_Listing_Adapter;

public class Dialog_testing extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.z_suscription);

        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_book_viewing);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        final String[] values = {"1.00 pm", "2.00 pm", "3.00 pm", "4.00 pm", "5.00 pm", "6.00 pm", "7.00 pm", "8.00 pm", "9.00 pm", "10.00 pm", "11.00 pm", "12.00 pm", "1.00 am", "2.00 am", "3.00 am", "4.00 am", "5.00 am", "6.00 am", "7.00 am", "8.00 am", "9.00 am", "10.00 am", "11.00 am", "12.00 am"};
        NumberPicker np_condo = (NumberPicker) dialog.findViewById(R.id.np_bookview);
        np_condo.setMinValue(0);
        np_condo.setMaxValue(values.length - 1);
        np_condo.setDisplayedValues(values);
        np_condo.setWrapSelectorWheel(true);


        NumberPicker np_condo2 = (NumberPicker) dialog.findViewById(R.id.np_bookview2);
        np_condo2.setMinValue(0);
        np_condo2.setMaxValue(values.length - 1);
        np_condo2.setDisplayedValues(values);
        np_condo2.setWrapSelectorWheel(true);

        final TextView txt_day = (TextView) dialog.findViewById(R.id.txt_day);

        txt_day.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();

                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(Dialog_testing.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                        txt_day.setText(dayOfMonth + "-" + (month + 1) + "-" + year);
                    }
                }, mYear, mMonth, mDay);
                datePickerDialog.show();

            }
        });
//        RecyclerView recycler_property_list = (RecyclerView)dialog. findViewById(R.id.recycler_suggest_listing);
//        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
//
//        recycler_property_list.setHasFixedSize(true);
//        recycler_property_list.setLayoutManager(layoutManager);
//        recycler_property_list.setAdapter(new Suggest_Listing_Adapter(this));

        dialog.show();

    }
}
