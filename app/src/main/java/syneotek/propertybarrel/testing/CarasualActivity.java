package syneotek.propertybarrel.testing;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import syneotek.propertybarrel.R;

public class CarasualActivity extends AppCompatActivity {

    CarouselView carouselView;
    int[] sampleImages = {R.drawable.index1, R.drawable.index2, R.drawable.index3, R.drawable.images4, R.drawable.images5, R.drawable.images7, R.drawable.images8};
    TextView txt_count;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carasual);

        carouselView = (CarouselView) findViewById(R.id.carouselView);
        carouselView.setPageCount(sampleImages.length);

        txt_count=(TextView)findViewById(R.id.txt_count);
        carouselView.setImageListener(imageListener);

        carouselView.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                int s=position+1;
                txt_count.setText(" "+s+"/"+sampleImages.length+" ");
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            imageView.setImageResource(sampleImages[position]);
        }
    };

}
