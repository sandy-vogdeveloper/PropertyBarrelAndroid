package syneotek.propertybarrel.fragment_agent;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;

import com.daimajia.swipe.util.Attributes;

import java.util.ArrayList;

import syneotek.propertybarrel.R;
import syneotek.propertybarrel.activity_agent.Agnt_MainActivity;
import syneotek.propertybarrel.adapter.Agent_List_Adapter;
import syneotek.propertybarrel.adapter_agent.Contact_List_Adapter;
import syneotek.propertybarrel.module.PropertyList_Items;
import syneotek.propertybarrel.util.RecyclerItemClickListener;


/**
 * Created by Syneotek1 on 9/12/2016.
 */
public class Frag_Contact extends Fragment {

    RecyclerView recycler_property_list;
    RecyclerView.LayoutManager layoutManager;

    View view;
    ArrayList<PropertyList_Items> arr_property_list = new ArrayList<PropertyList_Items>();

    ImageButton imgbtn_add_contact;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.frag_contacts, container, false);

        recycler_property_list = (RecyclerView) view.findViewById(R.id.recycler_property_list);
        layoutManager = new LinearLayoutManager(getActivity());

        imgbtn_add_contact=(ImageButton)view.findViewById(R.id.imgbtn_add_contact);

        for (int i = 0; i < 10; i++) {

            PropertyList_Items propertyList_items = new PropertyList_Items();
            propertyList_items.id = "" + i;
            propertyList_items.distance = i + " km";

            arr_property_list.add(propertyList_items);
        }

        // Creating Adapter object
        Contact_List_Adapter mAdapter = new Contact_List_Adapter(getActivity(), arr_property_list);

        // Setting Mode to Single to reveal bottom View for one item in List
        // Setting Mode to Mutliple to reveal bottom Views for multile items in List
        ((Contact_List_Adapter) mAdapter).setMode(Attributes.Mode.Single);

        recycler_property_list.setHasFixedSize(true);
        recycler_property_list.setLayoutManager(layoutManager);
        recycler_property_list.setAdapter(mAdapter);

//        recycler_property_list.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, final int position) {
//
//                ((Agnt_MainActivity) getActivity()).replaceFragment(new Frag_Specific_Contact());
//            }
//        }));

//        recycler_property_list.setAdapter(new Property_List2_Adapter(getActivity(),arr_property_list));
        imgbtn_add_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog1 = new Dialog(getActivity());
                dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog1.setContentView(R.layout.dialog_add_contact);
                dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

                dialog1.show();
            }
        });

        manageView();
        return view;
    }


    public void manageView() {


    }
}