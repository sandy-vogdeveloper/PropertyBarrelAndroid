package syneotek.propertybarrel.fragment_agent;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import java.util.Calendar;

import syneotek.propertybarrel.MainActivity;
import syneotek.propertybarrel.R;
import syneotek.propertybarrel.activity_agent.Agnt_MainActivity;
import syneotek.propertybarrel.fragment.Frag_Property_List;


/**
 * Created by Syneotek1 on 9/12/2016.
 */
public class Frag_Specific_Listing_Agent extends Fragment {

    View view;
    TextView txt_back;

    CarouselView carouselView;
    int[] sampleImages = {R.drawable.index1, R.drawable.index2, R.drawable.index3, R.drawable.images4, R.drawable.images5, R.drawable.images7, R.drawable.images8};
    TextView txt_count;

    ImageButton next, prev;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.frag_specific_listin, container, false);

        txt_back = (TextView) view.findViewById(R.id.txt_back);

        txt_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Agnt_MainActivity) getActivity()).replaceFragment(new Frag_Property_List_Agent());
            }
        });

        carouselView = (CarouselView) view.findViewById(R.id.carouselView);
        carouselView.setPageCount(sampleImages.length);

        txt_count = (TextView) view.findViewById(R.id.txt_count);
        carouselView.setImageListener(imageListener);

        carouselView.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                int s = position + 1;
                txt_count.setText(" " + s + "/" + sampleImages.length + " ");
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        next = (ImageButton) view.findViewById(R.id.next);
        prev = (ImageButton) view.findViewById(R.id.prev);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (carouselView.getCurrentItem() < sampleImages.length)
                carouselView.setCurrentItem(carouselView.getCurrentItem() + 1);

            }
        });

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (carouselView.getCurrentItem() > 0)
                    carouselView.setCurrentItem(carouselView.getCurrentItem() - 1);
            }
        });

        return view;
    }

    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            imageView.setImageResource(sampleImages[position]);
        }
    };



}