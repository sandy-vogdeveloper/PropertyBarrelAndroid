package syneotek.propertybarrel.fragment_agent;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import syneotek.propertybarrel.MainActivity;
import syneotek.propertybarrel.R;
import syneotek.propertybarrel.activity_agent.Agnt_MainActivity;
import syneotek.propertybarrel.adapter.Filter_Popup_Adapter;
import syneotek.propertybarrel.adapter.Property_List_Adapter;
import syneotek.propertybarrel.adapter_agent.Property_List_Adapter_Agent;
import syneotek.propertybarrel.fragment.Frag_Favorites_List;
import syneotek.propertybarrel.fragment.Frag_Property_List2;
import syneotek.propertybarrel.module.Module_Popup_Filter;


/**
 * Created by Syneotek1 on 9/12/2016.
 */
public class Frag_Property_List_Agent extends Fragment {

    RecyclerView recycler_property_list;
    RecyclerView.LayoutManager layoutManager;

    ImageButton imgbtn_refresh, imgbtn_home;
    View view;

    //---------------------for filter ---------------------------------------
    ImageButton filter_on_filter, filter_list2;
    RelativeLayout filter_view;
    RelativeLayout pro_type, price, beds, bath, open_hs, condo_fee, status, area;
    TextView txt_filt_site;

    ArrayList<Module_Popup_Filter> arr_prprty_type = new ArrayList<>();
    ArrayList<Module_Popup_Filter> arr_price = new ArrayList<>();
    ArrayList<Module_Popup_Filter> arr_beds = new ArrayList<>();
    ArrayList<Module_Popup_Filter> arr_bath = new ArrayList<>();
    ArrayList<Module_Popup_Filter> arr_status = new ArrayList<>();
    ArrayList<Module_Popup_Filter> arr_condo_fee = new ArrayList<>();

    RecyclerView rec_pro_type, rec_price, rec_beds, rec_bath, rec_status, rec_condofee;
    RecyclerView.LayoutManager layoutManager1, layoutManager2, layoutManager3, layoutManager4, layoutManager5, layoutManager6;

    TextView dia_cancel,dia_apply;
    Dialog dialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.frag_property_list_agent, container, false);

        recycler_property_list = (RecyclerView) view.findViewById(R.id.recycler_property_list);
        layoutManager = new LinearLayoutManager(getActivity());

        recycler_property_list.setHasFixedSize(true);
        recycler_property_list.setLayoutManager(layoutManager);
        recycler_property_list.setAdapter(new Property_List_Adapter_Agent(getActivity()));

        manageView();
        return view;
    }

    public void manageView() {

        txt_filt_site = (TextView) view.findViewById(R.id.txt_filt_site);
        filter_on_filter = (ImageButton) view.findViewById(R.id.filter_on_filter);
        filter_list2 = (ImageButton) view.findViewById(R.id.filter_list2);
        filter_view = (RelativeLayout) view.findViewById(R.id.filter_view);
        filter_view.setVisibility(View.GONE);

        txt_filt_site.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                filter_view.setVisibility(View.GONE);
            }
        });

        filter_on_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Animation fadeInAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.fadeout);
//                // Now Set your animation
//                filter_view.startAnimation(fadeInAnimation);
                filter_view.setVisibility(View.GONE);

            }
        });

        filter_list2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                filter_view.setVisibility(View.VISIBLE);
                Animation fadeInAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.fadein);
                // Now Set your animation
                filter_view.startAnimation(fadeInAnimation);

            }
        });

        pro_type = (RelativeLayout) view.findViewById(R.id.rele1);
        price = (RelativeLayout) view.findViewById(R.id.rele2);
        beds = (RelativeLayout) view.findViewById(R.id.rele3);
        bath = (RelativeLayout) view.findViewById(R.id.rele4);
        open_hs = (RelativeLayout) view.findViewById(R.id.rele5);
        condo_fee = (RelativeLayout) view.findViewById(R.id.rele6);
        status = (RelativeLayout) view.findViewById(R.id.rele7);
        area = (RelativeLayout) view.findViewById(R.id.rele8);

        pro_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dailog_pro_type();
            }
        });
        price.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dailog_price();
            }
        });
        beds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dailog_beds();
            }
        });
        bath.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dailog_bath();
            }
        });
        open_hs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        condo_fee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dailog_condofee();
            }
        });
        status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dailog_status();
            }
        });
        area.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        //-----------------------------------------------------------------

        imgbtn_refresh = (ImageButton) view.findViewById(R.id.imgbtn_refresh);
        imgbtn_home = (ImageButton) view.findViewById(R.id.imgbtn_home);

        imgbtn_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ((Agnt_MainActivity) getActivity()).replaceFragment(new Frag_Property_List2_Agent());

            }
        });

//        recycler_property_list.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, final int position) {
//
//                ((MainActivity) getActivity()).replaceFragment(new Frag_Specific_Listing());
//            }
//        }));

        imgbtn_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ((Agnt_MainActivity) getActivity()).replaceFragment(new Frag_Suggested_Prop_Agent());
            }
        });
    }


    public void dailog_pro_type() {

        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.filter_popup_prtytype);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        String arr_prty[] = {"Single-Family Home", "Condo/Townhouse", "Rental", "Lots", "Mobile Homes", "Multi-Family", "Commercial", "Industrial"};

        arr_prprty_type.clear();

        for (int i = 0; i < arr_prty.length; i++) {
            Module_Popup_Filter module_popup_filter = new Module_Popup_Filter();

            module_popup_filter.name = arr_prty[i];
            arr_prprty_type.add(module_popup_filter);
        }

        rec_pro_type = (RecyclerView) dialog.findViewById(R.id.settPop_recycel);
        layoutManager1 = new LinearLayoutManager(getActivity());
        rec_pro_type.setLayoutManager(layoutManager1);
        rec_pro_type.setHasFixedSize(true);
        rec_pro_type.setAdapter(new Filter_Popup_Adapter(getActivity(), arr_prprty_type));

        dia_cancel=(TextView)dialog.findViewById(R.id.dia_cancel);
        dia_apply=(TextView)dialog.findViewById(R.id.dia_apply);

        dia_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dia_apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Apply", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void dailog_price() {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.filter_popup_price);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        NumberPicker np_feeselect = (NumberPicker) dialog.findViewById(R.id.np_priceselct);
        np_feeselect.setMinValue(0);
        np_feeselect.setMaxValue(30000);
        np_feeselect.setValue(100);
        np_feeselect.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        np_feeselect.setWrapSelectorWheel(true);

        np_feeselect.setFormatter(new NumberPicker.Formatter() {
            @Override
            public String format(int value) {
                return String.valueOf(value * 1000);
            }
        });


        NumberPicker np_feeselect2 = (NumberPicker) dialog.findViewById(R.id.np_priceselct2);

        np_feeselect2.setMinValue(0);
        np_feeselect2.setMaxValue(30000);
        np_feeselect2.setValue(500);
        np_feeselect2.setDescendantFocusability(NumberPicker.FOCUS_AFTER_DESCENDANTS);
        np_feeselect2.setWrapSelectorWheel(true);

        np_feeselect2.setFormatter(new NumberPicker.Formatter() {
            @Override
            public String format(int value) {
                return String.valueOf(value * 1000);
            }
        });

        dia_cancel=(TextView)dialog.findViewById(R.id.dia_cancel);
        dia_apply=(TextView)dialog.findViewById(R.id.dia_apply);

        dia_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dia_apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Apply", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void dailog_beds() {

        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.filter_popup_bed);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        String arr_bed[] = {"1+","2+","3+","4+","6+"};

        arr_beds.clear();

        for (int i = 0; i < arr_bed.length; i++) {
            Module_Popup_Filter module_popup_filter = new Module_Popup_Filter();

            module_popup_filter.name = arr_bed[i];
            module_popup_filter.circl_status = 0;
            arr_beds.add(module_popup_filter);
        }

        rec_beds = (RecyclerView) dialog.findViewById(R.id.recy_popup_bed);
        layoutManager2 = new LinearLayoutManager(getActivity());
        rec_beds.setLayoutManager(layoutManager2);
        rec_beds.setHasFixedSize(true);
        rec_beds.setAdapter(new Filter_Popup_Adapter(getActivity(), arr_beds));

        dia_cancel=(TextView)dialog.findViewById(R.id.dia_cancel);
        dia_apply=(TextView)dialog.findViewById(R.id.dia_apply);

        dia_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dia_apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Apply", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void dailog_bath() {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.filter_popup_bath);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        String arr_baths[] = {"1+","2+","3+","4+","6+"};

        arr_bath.clear();

        for (int i = 0; i < arr_baths.length; i++) {
            Module_Popup_Filter module_popup_filter = new Module_Popup_Filter();

            module_popup_filter.name = arr_baths[i];
            arr_bath.add(module_popup_filter);
        }

        rec_bath = (RecyclerView) dialog.findViewById(R.id.rec_popup_bath);
        layoutManager3 = new LinearLayoutManager(getActivity());
        rec_bath.setLayoutManager(layoutManager3);
        rec_bath.setHasFixedSize(true);
        rec_bath.setAdapter(new Filter_Popup_Adapter(getActivity(), arr_bath));

        dia_cancel=(TextView)dialog.findViewById(R.id.dia_cancel);
        dia_apply=(TextView)dialog.findViewById(R.id.dia_apply);

        dia_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dia_apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Apply", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void dailog_condofee() {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.filter_popup_condofees);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        final String[] values = {"$100", "$200", "$300", "$400", "$500", "$600", "$700", "$800", "$900", "$100", "$1100", "$1200", "$1300", "$1400", "$1500", "$1600", "$1700", "$1800", "$1900", "$2000"};
        NumberPicker np_condo = (NumberPicker) dialog.findViewById(R.id.np_condofees);
        np_condo.setMinValue(0);
        np_condo.setMaxValue(values.length - 1);
        np_condo.setDisplayedValues(values);
        np_condo.setWrapSelectorWheel(true);

        final String[] values2 = {"$100", "$200", "$300", "$400", "$500", "$600", "$700", "$800", "$900", "$100", "$1100", "$1200", "$1300", "$1400", "$1500", "$1600", "$1700", "$1800", "$1900", "$2000"};
        NumberPicker np_condo2 = (NumberPicker) dialog.findViewById(R.id.np_condofees2);
        np_condo2.setMinValue(0);
        np_condo2.setMaxValue(values2.length - 1);
        np_condo2.setDisplayedValues(values2);
        np_condo2.setWrapSelectorWheel(true);

        dia_cancel=(TextView)dialog.findViewById(R.id.dia_cancel);
        dia_apply=(TextView)dialog.findViewById(R.id.dia_apply);

        dia_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dia_apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Apply", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void dailog_status() {

        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.filter_popup_status);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        String arr_stat[]={"Any","Active","Pending","Sold","Back Up Offer"};

        arr_status.clear();

        for (int i = 0; i < arr_stat.length; i++) {
            Module_Popup_Filter module_popup_filter = new Module_Popup_Filter();

            module_popup_filter.name = arr_stat[i];
            arr_status.add(module_popup_filter);
        }

        rec_status = (RecyclerView) dialog.findViewById(R.id.rec_popup_status);
        layoutManager4 = new LinearLayoutManager(getActivity());
        rec_status.setLayoutManager(layoutManager4);
        rec_status.setHasFixedSize(true);
        rec_status.setAdapter(new Filter_Popup_Adapter(getActivity(), arr_status));

        dia_cancel=(TextView)dialog.findViewById(R.id.dia_cancel);
        dia_apply=(TextView)dialog.findViewById(R.id.dia_apply);

        dia_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dia_apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Apply", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
        dialog.show();
    }


}