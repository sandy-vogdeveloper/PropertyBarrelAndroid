package syneotek.propertybarrel.fragment_agent;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import syneotek.propertybarrel.R;
import syneotek.propertybarrel.activity_agent.Agnt_MainActivity;
import syneotek.propertybarrel.adapter_agent.Profile_Hori_Adapter;


/**
 * Created by Syneotek1 on 9/12/2016.
 */
public class Frag_Specific_Contact extends Fragment {

    View view;
    RecyclerView rv_suggest,rv_favorite;
    TextView txt_back;
    Button btn_con_sold,btn_sold;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.frag_specific_contact, container, false);

        rv_suggest = (RecyclerView) view.findViewById(R.id.suggest_recycel);
        rv_favorite= (RecyclerView) view.findViewById(R.id.fav_recycel);

        RecyclerView.LayoutManager mLayoutmanager = new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false);
        rv_suggest.setLayoutManager(mLayoutmanager);
        rv_suggest.setItemAnimator(new DefaultItemAnimator());
        rv_suggest.setAdapter(new Profile_Hori_Adapter(getActivity()));

        RecyclerView.LayoutManager mLayoutmanager2=new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false);
        rv_favorite.setLayoutManager(mLayoutmanager2);
        rv_favorite.setItemAnimator(new DefaultItemAnimator());
        rv_favorite.setAdapter(new Profile_Hori_Adapter(getActivity()));


        txt_back = (TextView) view.findViewById(R.id.txt_back);

        txt_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Agnt_MainActivity) getActivity()).replaceFragment(new Frag_Contact());
            }
        });

        btn_con_sold=(Button)view.findViewById(R.id.btn_con_sold);
        btn_sold=(Button)view.findViewById(R.id.btn_sold);

        btn_con_sold.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                specialInfoDialog();
            }
        });
        btn_sold.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                specialInfoDialog();
            }
        });

        return view;
    }

    public void specialInfoDialog()
    {
        Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_special_info);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.show();
    }

}