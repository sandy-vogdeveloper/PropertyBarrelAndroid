package syneotek.propertybarrel.adapter_agent;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import syneotek.propertybarrel.R;

/**
 * Created by Syneotek on 02/05/2017.
 */

public class Suggest_Listing_Adapter extends RecyclerView.Adapter<Suggest_Listing_Adapter.MyViewHolder> {

    Activity activity;

    public Suggest_Listing_Adapter(Activity activity) {
        this.activity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adpt_suggest_listing_items, parent, false);

        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txt_menu_name;
        ImageView img_menu;
        RelativeLayout swipe;

        public MyViewHolder(View view) {
            super(view);

//            txt_menu_name=(TextView)view.findViewById(R.id.txt_menu_name);
//            img_menu=(ImageView) view.findViewById(R.id.img_menu);
            swipe = (RelativeLayout) view.findViewById(R.id.swipe);

        }
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

//        final Model model = mModelList.get(position);
//        holder.textView.setText(model.getText());
//        holder.view.setBackgroundColor(model.isSelected() ? Color.CYAN : Color.WHITE);
        holder.swipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                model.setSelected(!model.isSelected());

                    holder.swipe.setBackgroundColor(Color.GRAY);
            }
        });
//        holder.img_menu.setImageResource(arr_menu_image[position]);
//        holder.txt_menu_name.setText(arr_menu_name[position]);
    }

    @Override
    public int getItemCount() {
        return 10;
//        return arr_menu_name.length;
    }
}
