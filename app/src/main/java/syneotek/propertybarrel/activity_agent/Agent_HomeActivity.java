package syneotek.propertybarrel.activity_agent;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import syneotek.propertybarrel.MainActivity;
import syneotek.propertybarrel.R;
import syneotek.propertybarrel.adapter.Menu_Adapter;
import syneotek.propertybarrel.util.RecyclerItemClickListener;

public class Agent_HomeActivity extends AppCompatActivity {

    RecyclerView recycler_menu;
    RecyclerView.LayoutManager layoutManager;

    String[] arr_menu_name = {"Property Search", "Showings", "Messaging", "Menu", "Mortgage Calculator", "Contact", "Map", "Suggested Properties", "Settings", "Share App"};
    int[] arr_menu_image = {R.drawable.men_search, R.drawable.showing, R.drawable.men_msg, R.drawable.men_menu,
            R.drawable.men_calc, R.drawable.men_agent, R.drawable.men_mapicon, R.drawable.men_home, R.drawable.men_setting, R.drawable.men_shearapp};

    int menu_count = 8;

    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agent__home);


        recycler_menu = (RecyclerView) findViewById(R.id.recycler_menu);
        layoutManager = new GridLayoutManager(this, 4);

        recycler_menu.setHasFixedSize(true);
        recycler_menu.setLayoutManager(layoutManager);
        recycler_menu.setAdapter(new Menu_Adapter(Agent_HomeActivity.this, arr_menu_name, arr_menu_image, menu_count));

        recycler_menu.addOnItemTouchListener(new RecyclerItemClickListener(Agent_HomeActivity.this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, final int position) {

                switch (position) {

                    case 0:
                        intent = new Intent(Agent_HomeActivity.this, Agnt_MainActivity.class);
                        startActivity(intent);
                        break;

                    case 3:
                        if (menu_count == 8 || menu_count == 4)
                            menu_count = 10;
                        else
                            menu_count = 8;

                        recycler_menu.setAdapter(new Menu_Adapter(Agent_HomeActivity.this, arr_menu_name, arr_menu_image, menu_count));
                        break;

                    case 4:
                        intent = new Intent(Agent_HomeActivity.this, Agnt_MainActivity.class);
                        startActivity(intent);
                        break;

                    case 5:
                        intent = new Intent(Agent_HomeActivity.this, Agnt_MainActivity.class);
                        startActivity(intent);
                        break;

                    case 6:
                        intent = new Intent(Agent_HomeActivity.this, Agnt_MainActivity.class);
                        startActivity(intent);
                        break;

                    case 7:
                        intent = new Intent(Agent_HomeActivity.this, Agnt_MainActivity.class);
                        startActivity(intent);
                        break;

                    case 8:
                        intent = new Intent(Agent_HomeActivity.this, Agnt_MainActivity.class);
                        startActivity(intent);
                        break;

                }

            }
        }));

    }
}
